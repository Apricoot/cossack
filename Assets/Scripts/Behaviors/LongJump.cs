﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongJump : Jump
{

    public float longJumpDelay = .15f;
    public float longJumpMyltiplier = 1.5f;
    public bool canLongJump;
    public bool isLongJump;


   protected override void Update()
    {
        var canJump = inputState.GetButtonValue(inputButtons[0]);
        var holdTime = inputState.GetButtonHoldTime(inputButtons[0]);

        if (!canJump)
        {
            canLongJump = false;
        }
        if(collisionState.standing && isLongJump)
        {
            isLongJump = false;
        }


        base.Update();

        if(canLongJump && !collisionState.standing && holdTime > longJumpDelay)
        {
            var vel = body2d.velocity;
            body2d.velocity = new Vector2(vel.x, jumpSpeed *longJumpMyltiplier);
            canLongJump = false;
            isLongJump = true;

        }
    }

    protected override void OnJump()
    {
        base.OnJump();
        canLongJump = true;
    }
}
