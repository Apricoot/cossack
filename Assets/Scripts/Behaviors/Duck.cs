﻿using UnityEngine;
using System.Collections;

public class Duck : AbstractBehavior
{

    public float scale = .5f;
    public bool ducking;
    public float centerOffsetY = 0f;

    private CircleCollider2D circleCollider;
    private BoxCollider2D boxCollder;
    private Vector2 originalCenter;
    private Vector2 originalCenterB;
    protected override void Awake()
    {
        base.Awake();

        circleCollider = GetComponent<CircleCollider2D>();
        boxCollder = GetComponent<BoxCollider2D>();
        originalCenter = circleCollider.offset;
        originalCenterB = boxCollder.offset;
    }

    protected virtual void OnDuck(bool value)
    {

        ducking = value;

        ToggleScripts(!ducking);

        var size = circleCollider.radius;
        var sizeB = boxCollder.size;

        float newOffsetY;
        float sizeReciprocal;

        float newOffsetYB;
        float sizeRiciprocalB;

        if (ducking)
        {
            sizeReciprocal = scale;
            sizeRiciprocalB = scale;
            newOffsetY = circleCollider.offset.y - size / 2 + centerOffsetY;
            newOffsetYB = boxCollder.offset.y - size / 2 + centerOffsetY;
        }
        else
        {
            sizeReciprocal = 1 / scale;
            sizeRiciprocalB = 1 / scale;
            newOffsetY = originalCenter.y;
            newOffsetYB = originalCenterB.y;
        }

        size = size * sizeReciprocal;
        sizeB = sizeB * sizeRiciprocalB;
        circleCollider.radius = size;
        boxCollder.size = sizeB;
        circleCollider.offset = new Vector2(circleCollider.offset.x, newOffsetY);
        boxCollder.offset = new Vector2(boxCollder.offset.x, newOffsetYB);

    }

    // Update is called once per frame
    void Update()
    {

        var canDuck = inputState.GetButtonValue(inputButtons[0]);
        if (canDuck && collisionState.standing && !ducking)
        {
            OnDuck(true);
        }
        else if (ducking && !canDuck)
        {
            OnDuck(false);
        }

    }
}