﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingThruePlatform : MonoBehaviour
{
    // store the layer the player is on (setup in Awake)
    int _playerLayer;

    // number of layer that Platforms are on (setup in Awake)
    int _platformLayer;

    Rigidbody2D _rigidbody;

    float _vy;
    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();

        // determine the player's specified layer
        _playerLayer = this.gameObject.layer;

        // determine the platform's specified layer
        _platformLayer = LayerMask.NameToLayer("Platform");
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        // get the current vertical velocity from the rigidbody component
        _vy = _rigidbody.velocity.y;

        // if moving up then don't collide with platform layer
        // this allows the player to jump up through things on the platform layer
        // NOTE: requires the platforms to be on a layer named "Platform"
        Physics2D.IgnoreLayerCollision(_playerLayer, _platformLayer, (_vy > 0.0f));
    }
}
