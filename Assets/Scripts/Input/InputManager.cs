﻿using UnityEngine;
using System.Collections;

/// <summary>
/// исспользуется для задания кнопок ввода пользователя
/// </summary>
public enum Buttons{
	Right,
	Left,
	Up,
	Down,
	A,
	B
}
/// <summary>
/// исспользуется для задания значения оси
/// </summary>
public enum Condition{
	GreaterThan,
	LessThan
}

//класс для 
//записи значения по оси
//и какая кнопка, будет массивом
[System.Serializable]
public class InputAxisState{
    //имя оси
	public string axisName;
    /// <summary>
    /// значение оси ввода
    /// </summary>
	public float offValue;
    /// <summary>
    /// переменная хранящая кнопки
    /// </summary>
	public Buttons button;
    /// <summary>
    /// хранит состояние выбранной кнопки, или оси
    /// </summary>
	public Condition condition;

	public bool value{

		get{
			var val = Input.GetAxisRaw(axisName);

			switch(condition){
			case Condition.GreaterThan:
				return val > offValue;
			case Condition.LessThan:
				return val < offValue;
			}

			return false;
		}

	}
}
/// <summary>
/// исспользуется для конвертирования инпутманагера в понятный для кода
/// </summary>
public class InputManager : MonoBehaviour {
    /// <summary>
    /// массив, хранящий
    /// </summary>
	public InputAxisState[] inputs;
	public InputState inputState;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		foreach (var input in inputs) {
			inputState.SetButtonValue(input.button, input.value);
		}
	}
}
